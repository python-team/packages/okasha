okasha (0.3.0-2) unstable; urgency=medium

  * Team upload.
  * Add missing build-depends on python3-setuptools (Closes: #1080683)

 -- Alexandre Detiste <tchet@debian.org>  Fri, 07 Feb 2025 22:53:04 +0100

okasha (0.3.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Add debian/gbp.conf
  * Update debian/watch
  * Update standards version to 4.6.2, no changes needed

 -- Jeremy Bicha <jbicha@ubuntu.com>  Fri, 20 Jan 2023 09:38:53 -0500

okasha (0.2.4-6) unstable; urgency=medium

  * New upload, this time with binary packages

 -- Sandro Tosi <morph@debian.org>  Fri, 17 Jun 2022 17:49:03 -0400

okasha (0.2.4-5) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Update standards version to 4.5.0, no changes needed.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Wed, 04 May 2022 16:23:26 -0400

okasha (0.2.4-4) unstable; urgency=medium

  * Add Breaks+Replaces: python-okasha-examples (Closes: #940198)
  * py3.diff: add fix for string + utf8 concatenation

 -- أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@users.sourceforge.net>  Sat, 14 Sep 2019 02:55:38 +0200

okasha (0.2.4-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Fix Format URL to correct one
  * d/control: Remove ancient X-Python-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.

  [ أحمد المحمودي (Ahmed El-Mahmoudy) ]
  * Switch to Python 3 (Closes: #937185)
    + Add py3.diff patch to migrate to Python 3
  * Do not install kid template
  * Bumped to compat level 12
  * Update to standards version 4.4.0
  * Add upstream metadata
  * Update homepage URL
  * Update copyright years

 -- أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@users.sourceforge.net>  Tue, 03 Sep 2019 20:26:03 +0200

okasha (0.2.4-2) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ أحمد المحمودي (Ahmed El-Mahmoudy) ]
  * Update my email address
  * debian/copyright:
    + Update copyright years
    + Switch to DEP-5 copyright format
    + Fix a couple of file copyright  entries
    + Corrected path of GPL-1 license
  * Update Standards-Version to 4.1.1
  * Bumped compat level to 10
  * Update URL in watch file

 -- أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@users.sourceforge.net>  Fri, 13 Oct 2017 04:44:12 +0200

okasha (0.2.4-1) unstable; urgency=low

  * New upstream release.
  * debian/python-okasha-examples.install: Install demo-themes instead of
    files & templates.
  * Updated debian/python-okasha-examples.links
  * debian/copyright: update copyright years
  * debian/control:
    + Put python-all in Build-Depends rather than Build-Depends-Indep
      (Closes: #606375)
    + Bumped Standards-Version to 3.9.2, no changes needed.
  * Bumped compat level to 8.

 -- أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@sabily.org>  Thu, 28 Jul 2011 14:47:44 +0200

okasha (0.2.1-2) unstable; urgency=low

  * Team upload.
  * Rebuild to add Python 2.7 support

 -- Piotr Ożarowski <piotr@debian.org>  Sun, 08 May 2011 16:45:41 +0200

okasha (0.2.1-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    + Fix typos in extended descriptions.
    + Added entry about Bottle-based templates in extended description.
    + Added X-Python-Version field.
    + Bumped Standards-Version to 3.9.1, no changes needed.
    + Removed python-support from Build-Deps
    + Bumped python-all Build-Dep to (>= 2.6.6-2)
    + Added Breaks: ${python:Breaks}, to avoid getting
      python (<= <UNSUPPORTED VERSION>) in Depends.
  * debian/rules: added --with python2 to dh call.
  * Removed debian/pyversions
  * debian/copyright: Added entry for okasha/bottleTemplateSegment.py

 -- أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@sabily.org>  Thu, 02 Dec 2010 18:07:22 +0200

okasha (0.1.0-2) unstable; urgency=low

  * debian/copyright:
    + Change license of debian packaging to GPL-3+ or Waqf Public License to
      avoid incompatible license issues.
    + Clarify the copyright of files/okasha-docbook.css

 -- أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@sabily.org>  Sun, 11 Jul 2010 05:41:48 +0300

okasha (0.1.0-1) unstable; urgency=low

  * Initial release (Closes: #585999)

 -- أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@sabily.org>  Tue, 06 Jul 2010 19:39:24 +0300
